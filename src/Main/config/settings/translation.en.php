<?php

return [
    'translation_module' => [
        'main' => 'Main page',
        'authentication' => 'Authentication',
        'authorization' => 'Authorization',
    ]
];
