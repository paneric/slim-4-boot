<?php

declare(strict_types=1);

use App\Main\Controller\MainController;
use App\Main\Service\MainService;
use DI\Container;
use Paneric\Session\SessionWrapper;
use Twig\Environment as Twig;

return [
    MainController::class => function (Container $container): MainController
    {
        return new MainController(
            $container->get(Twig::class),
            $container->get(SessionWrapper::class),
            $container->get(MainService::class)
        );
    },
];
