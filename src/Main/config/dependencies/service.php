<?php

declare(strict_types=1);

use App\Main\Service\MainService;
use DI\Container;

return [
    MainService::class => function (Container $container): MainService
    {
        return new MainService();
    },
];
