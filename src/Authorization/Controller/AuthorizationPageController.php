<?php

declare(strict_types=1);

namespace App\Authorization\Controller;

use Paneric\Controller\AppController;
use App\Authorization\Service\AuthorizationPageService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class AuthorizationPageController extends AppController
{
    private $service;

    public function __construct(
        AuthorizationPageService $authorizationPageService,
        Twig $twig,
        SessionInterface $session
    ) {
        parent::__construct($twig, $session);

        $this->service = $authorizationPageService;
    }

    public function index(Response $response): Response
    {
        return $this->render(
            $response,
            '@authorization/index.html.twig',
            $this->service->index()
        );
    }
}