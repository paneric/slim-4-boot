<?php

declare(strict_types=1);

namespace App\Authorization\Service;

class AuthorizationPageService
{
    public function index(): array
    {
        return [];
    }
}
