<?php

declare(strict_types=1);

$validationModule = [
    'validation' => [
    ],
];

$validationAutho = include ROOT_FOLDER . 'vendor/paneric/authorization/validation.php';

$validation['validation'] = array_merge(
    $validationModule['validation'],
    $validationAutho['validation'],
);

return $validation;
