<?php

return [
    'twig-builder-app' => [
        'templates_dirs' => [
            'error' => APP_FOLDER . 'Error/templates/',
            'authorization' => ROOT_FOLDER . 'vendor/paneric/authorization/src/templates/',
            'module' => APP_FOLDER . 'Authorization/templates/',
            'app' => APP_FOLDER . 'templates/',
        ],
        'options' => [
            'debug' => true, /* "prod" false */
            'charset' => 'UTF-8',
            'strict_variables' => false,
            'autoescape' => 'html',
            'cache' => false, /* "prod" ROOT_FOLDER.'/var/cache/twig'*/
            'auto_reload' => null,
            'optimizations' => -1,
        ],
    ]
];
