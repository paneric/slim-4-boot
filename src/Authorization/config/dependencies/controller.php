<?php

declare(strict_types=1);

use App\Authorization\Controller\AuthorizationPageController;
use App\Authorization\Service\AuthorizationPageService;
use DI\Container;
use Paneric\Session\SessionWrapper;
use Twig\Environment as Twig;

return [
    AuthorizationPageController::class => function (Container $container): AuthorizationPageController
    {
        return new AuthorizationPageController(
            $container->get(AuthorizationPageService::class),
            $container->get(Twig::class),
            $container->get(SessionWrapper::class)
        );
    },
];
