<?php

declare(strict_types=1);

use App\Authorization\Service\AuthorizationPageService;
use DI\Container;

return [
    AuthorizationPageService::class => function (Container $container): AuthorizationPageService {
        return new AuthorizationPageService();
    },
];
