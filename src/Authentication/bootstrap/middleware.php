<?php

declare(strict_types=1);

use DI\DependencyException;
use DI\NotFoundException;
use Middlewares\TrailingSlash;
use Paneric\Authorization\AuthorizationMiddleware;
use Paneric\Middleware\AuthenticationMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Session\SessionMiddleware;
use Paneric\Middleware\UriMiddleware;

try {
    $app->addMiddleware($container->get(AuthorizationMiddleware::class));
    $app->addMiddleware($container->get(AuthenticationMiddleware::class));
//    $app->add(new TrailingSlash(true)); // true adds the trailing slash (false removes it)
    $app->addMiddleware($container->get(UriMiddleware::class));
    $app->addMiddleware($container->get(RouteMiddleware::class));
    $app->addMiddleware($container->get(SessionMiddleware::class));
} catch (DependencyException $e) {
    echo $e->getMessage();
} catch (NotFoundException $e) {
    echo $e->getMessage();
}

$app->addRoutingMiddleware();
