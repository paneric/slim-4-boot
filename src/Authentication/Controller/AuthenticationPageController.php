<?php

declare(strict_types=1);

namespace App\Authentication\Controller;

use Paneric\Controller\AppController;
use App\Authentication\Service\AuthenticationPageService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class AuthenticationPageController extends AppController
{
    private $service;

    public function __construct(
        AuthenticationPageService $authenticationPageService,
        Twig $twig,
        SessionInterface $session
    ) {
        parent::__construct($twig, $session);

        $this->service = $authenticationPageService;
    }

    public function index(Response $response): Response
    {
        return $this->render(
            $response,
            '@authentication/index.html.twig',
            $this->service->index()
        );
    }
}
