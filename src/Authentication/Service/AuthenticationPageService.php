<?php

declare(strict_types=1);

namespace App\Authentication\Service;

class AuthenticationPageService
{
    public function index(): array
    {
        return [];
    }
}
