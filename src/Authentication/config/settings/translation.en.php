<?php

$translationModule = [
    'translation_module' => [
        'message_title' => 'slim-4-boot message'
    ],
];

$translationPackage = include ROOT_FOLDER . 'vendor/paneric/authentication/translation.en.php';

$translationModule['translation_module'] = array_merge($translationModule['translation_module'], $translationPackage);

return $translationModule;
