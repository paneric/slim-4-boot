<?php

declare(strict_types=1);

use App\Authentication\Controller\AuthenticationPageController;
use App\Authentication\Service\AuthenticationPageService;
use DI\Container;
use Paneric\Session\SessionWrapper;
use Twig\Environment as Twig;

return [
    AuthenticationPageController::class => function (Container $container): AuthenticationPageController
    {
        return new AuthenticationPageController(
            $container->get(AuthenticationPageService::class),
            $container->get(Twig::class),
            $container->get(SessionWrapper::class)
        );
    },
];
