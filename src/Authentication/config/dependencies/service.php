<?php

declare(strict_types=1);

use App\Authentication\Service\AuthenticationPageService;
use DI\Container;

return [
    AuthenticationPageService::class => function (Container $container): AuthenticationPageService {
        return new AuthenticationPageService();
    },
];
