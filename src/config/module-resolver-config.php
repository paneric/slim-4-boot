<?php

return [
    'default_route_key' => 'main',
    'local_map' => ['fr', 'nl', 'de', 'en', 'pl'],
    'module_map' => [
        'error' => 'Error',
        'main' => 'Main',
        'authe' => 'Authentication',
        'autho' => 'Authorization',
    ],
];
