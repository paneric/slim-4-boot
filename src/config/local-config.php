<?php

return [
    'name' => 'slim-4-boot',
    'default_local' => 'en',
    'expire' => time() + 60 * 60 * 24 * 120, // 120 days
    'path' => '/public',
    'domain' => '127.0.0.1:8080',
    'security' => false,
];
