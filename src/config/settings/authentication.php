<?php

return [
    'recaptcha-middleware' => [
        'site_key' => '6LceLdsUAAAAALkAvJtzFRk7YR1QZwJjPN5CoxoT',
        'url_api_script' => '<script src="https://www.google.com/recaptcha/api.js?render=%s"></script>',
        'url_site_verify' => 'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s',
        'file_path' => ROOT_FOLDER . 'temp2.txt',
        'score' => 0.1,
    ],
    'credential-service' =>[
        'role_id' => 9,
        'csrf_hash_field_name' => 'csrf_hash',
    ],
    'mailer' => [
        'smtp_debug' => 0,
        'host' => 'smtp.mailtrap.io',
        'smtp_auth' => true,
        'username' => '27f054dd15af4f',
        'password' => '15974543b4f6d6',
        'smtp_secure' => 'tls',
        'port' => 2525,
        'html' => true,
        'set_from_address' => '5f10887ab7-9afe04@inbox.mailtrap.io',
        'set_from_name' => 'Mailer',
    ],
    'mailer-service' => [
        'random_string_length' => 128,

        'activation_url' => 'http://127.0.0.1:8080/authe',
        'activation_time_delay' => 24,

        'reset_url' => 'http://127.0.0.1:8080/authe',
        'reset_time_delay' => 24,

        'mail_content' => [
            'activation_email_subject' => [
                'en' => 'Account activation automatic message.'
            ],
            'activation_email_message' => [
                'en' => 'Please click the following link within %sh to activate your account: %s/%s/%s/activate .'
            ],
            'reset_email_subject' => [
                'en' => 'Password reset automatic message.'
            ],
            'reset_email_message' => [
                'en' => 'Please click the following link within %sh to reset your account password: %s/%s/%s/reset .'
            ],
        ],
    ],
];