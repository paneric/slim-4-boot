<?php

return [
    'translation_app' => [
        'service_name' => 'Slim 4 boot',
        'authentication' => 'Authentication',
        'authorization' => 'Authorization',

        'error' => 'Error',
        'Not found.' => 'Page not found.',

        'log_in' => 'Log in',
        'restore' => 'Restore',
        'log_out' => 'Log out',
        'subscribe' => 'Subscribe',
        'unsubscribe' => 'Unsubscribe',
    ],
];
