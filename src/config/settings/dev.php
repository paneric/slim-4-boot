<?php

return [
    'pdo-wrapper' => [
        'limit' => 10,
        'host' => 'localhost',
        'dbName' => 'slim_4_boot',
        'charset' => 'utf8',
        'user' => 'toor',
        'password' => 'toor',
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_WARNING, //\PDO::ERRMODE_SILENT, \PDO::ERRMODE_WARNING, \PDO::ERRMODE_EXCEPTION
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_FOUND_ROWS   => false, // true -> matched, false -> affected
        ],
    ],
    'guard' => [
        'file_path' => ROOT_FOLDER . 'temp.txt',
        'algo_password' => PASSWORD_BCRYPT,
        'options_algo_password' => [
            'cost' => 10,
        ],
        'algo_hash' => 'sha512',
        'unique_id_prefix' => '',
        'unique_id_more_entropy' => true,
    ],
];
