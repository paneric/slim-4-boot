<?php

declare(strict_types=1);

use Paneric\Twig\Extension\ArrayExtension;
use Paneric\Twig\Extension\CSRFExtension;
use Paneric\Twig\Extension\HtmlExtension;
use Paneric\Twig\Extension\StringExtension;
use Paneric\Twig\Extension\ValidationExtension;
use DI\Container;
use GuzzleHttp\Psr7\Uri;
use Paneric\Guard\Guard;
use Paneric\PdoWrapper\DataPreparator;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\PDOBuilder;
use Paneric\PdoWrapper\QueryBuilder;
use Paneric\PdoWrapper\SequencePreparator;
use Paneric\Session\PDO\PDOAdapter;
use Paneric\Session\PDO\SessionRepository;
use Paneric\Session\SessionWrapper;
use Paneric\Twig\Extension\SessionExtension;
use Paneric\Twig\Extension\TwigExtension;
use Paneric\Twig\TwigBuilder;
use Paneric\Validation\Validator;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Translator;
use Twig\Environment as Twig;
use Twig\Extension\DebugExtension;

$authe = include ROOT_FOLDER . 'vendor/paneric/authentication/package.php';
$autho = include ROOT_FOLDER . 'vendor/paneric/authorization/package-pdo.php';

$appPackage = [
    Manager::class => function (Container $container): Manager
    {
        $pdoBuilder = new PDOBuilder();

        return new Manager(
            $pdoBuilder->build($container->get('pdo-wrapper')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },

    Guard::class => function (Container $container): Guard
    {
        $randomFactory = new RandomLib\Factory;

        return new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            $container->get('guard')
        );
    },

    SessionWrapper::class => function (Container $container): SessionWrapper
    {
        $pdoAdapter = new PDOAdapter(
            $container->get(Manager::class),
            new SessionRepository($container->get(Manager::class)),
            $container->get(Guard::class)
        );

        return new SessionWrapper(
            $container->get('session-wrapper'),
            $pdoAdapter
        );
    },

    Translator::class => function (Container $container): Translator
    {
        $translator = new Translator((string) $container->get('local'));
        $translator->addLoader('array', new ArrayLoader());
        $translator->addResource(
            'array',
            array_merge($container->get('translation_app'), $container->get('translation_module')),
            (string) $container->get('local')
        );

        return $translator;
    },

    Twig::class => function (Container $container): Twig
    {
        $twigBuilder = new TwigBuilder();

        $twigBuilderSettings = [];

        if ($container->has('twig-builder-module')) {
            $twigBuilderSettings = $container->get('twig-builder-module');
        }

        if ($twigBuilderSettings === [] && $container->has('twig-builder-app')) {
            $twigBuilderSettings = $container->get('twig-builder-app');
        }

        $environment = $twigBuilder->build($twigBuilderSettings);

        $twigExtension = new TwigExtension($container->get('route_parser_interface'), $container->get(Uri::class));
        $environment->addExtension($twigExtension);

        $sessionExtension = new SessionExtension(
            $container->get(SessionWrapper::class),
            $container->get(Translator::class)
        );
        $environment->addExtension($sessionExtension);

        $translationExtension = new TranslationExtension($container->get(Translator::class));
        $environment->addExtension($translationExtension);

        $environment->addExtension($container->get(CSRFExtension::class));

        $validationExtension = new ValidationExtension($container->get(Validator::class));
        $environment->addExtension($validationExtension);

        $environment->addExtension(new DebugExtension());
        $environment->addExtension(new ArrayExtension());
        $environment->addExtension(new StringExtension());
        $environment->addExtension(new HtmlExtension());

        return $environment;
    },
];

return array_merge($authe, $autho, $appPackage);
