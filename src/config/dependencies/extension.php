<?php

declare(strict_types=1);

use Paneric\Twig\Extension\CSRFExtension;
use DI\Container;
use Paneric\Guard\Guard;
use Paneric\Session\SessionWrapper;

return [
    CSRFExtension::class => function (Container $container): CSRFExtension
    {
        return new CSRFExtension(
            $container->get(SessionWrapper::class),
            $container->get(Guard::class),
            $container->get('csrf')
        );
    },
];