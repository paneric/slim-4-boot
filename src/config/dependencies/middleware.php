<?php

declare(strict_types=1);

use Paneric\Middleware\AuthenticationMiddleware;
use Paneric\Authorization\AuthorizationMiddleware;
use Paneric\Authorization\PDO\Query\RoleQuery;
use Paneric\Authorization\PDO\Repository\PrivilegeRepository;
use Paneric\Authorization\PDO\Repository\RoleRepository;
use Paneric\Middleware\CSRFMiddleware;
use DI\Container;
use Paneric\Guard\Guard;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Validation\Validator;
use Paneric\Validation\ValidatorBuilder;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Session\SessionMiddleware;
use Paneric\Middleware\UriMiddleware;
use Paneric\Session\SessionWrapper;

return [
    RouteMiddleware::class => function (Container $container): RouteMiddleware
    {
        return new RouteMiddleware($container);
    },

    UriMiddleware::class => function (Container $container): UriMiddleware
    {
        return new UriMiddleware($container);
    },

    SessionMiddleware::class => function (Container $container): SessionMiddleware
    {
        return new SessionMiddleware($container);
    },

    AuthenticationMiddleware::class => function (Container $container): AuthenticationMiddleware
    {
        return new AuthenticationMiddleware($container->get(SessionWrapper::class));
    },

    AuthorizationMiddleware::class => function (Container $container): AuthorizationMiddleware
    {
        return new AuthorizationMiddleware(
            $container->get(RoleRepository::class),
            $container->get(PrivilegeRepository::class),
            $container->get(SessionWrapper::class),
        );
    },

    Validator::class => function (Container $container): Validator
    {
        $validatorBuilder = new ValidatorBuilder();

        return $validatorBuilder->build(
            (string) $container->get('local')
        );
    },

    ValidationMiddleware::class => function (Container $container): ValidationMiddleware
    {
        return new ValidationMiddleware(
            $container->get(Validator::class),
            (array) $container->get('validation')
        );
    },

    CSRFMiddleware::class => function (Container $container): CSRFMiddleware
    {
        $config = $container->get('csrf');

        return new CSRFMiddleware(
            $container->get(SessionWrapper::class),
            $container->get(Guard::class),
            $config
        );
    },
];
