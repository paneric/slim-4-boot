<?php

declare(strict_types=1);

use App\Error\Controller\ErrorController;
use DI\Container;
use Paneric\Session\SessionWrapper;
use Twig\Environment as Twig;

return [
    ErrorController::class => function (Container $container): ErrorController
    {
        return new ErrorController(
            $container->get(Twig::class),
            $container->get(SessionWrapper::class)
        );
    },
];
