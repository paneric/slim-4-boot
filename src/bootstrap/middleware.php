<?php

declare(strict_types=1);

// Define Custom Error Handler
use App\Error\Controller\ErrorController;
use Psr\Http\Message\ServerRequestInterface;

$customErrorHandler = function (
    ServerRequestInterface $request,
    Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails
) use ($app) {
    $payload = [
        'message' => $exception->getMessage(),
        'code' => $exception->getCode()
    ];

    $response = $app->getResponseFactory()->createResponse();

    return $app->getContainer()->get(ErrorController::class)->index($response, $payload);
};

// always last one
$errorMiddleware = $app->addErrorMiddleware(
    true,
    true,
    true
);
$errorMiddleware->setDefaultErrorHandler($customErrorHandler);
