<?php

declare(strict_types=1);

use App\Authentication\Controller\AuthenticationPageController;
use App\Authorization\Controller\AuthorizationPageController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Main\Controller\MainController;

$app->get('/', function(Request $request, Response $response) {
    return $this->get(MainController::class)->index($request, $response);
})->setName('main.index');

$app->get('/authe', function(Request $request, Response $response) {
    return $this->get(AuthenticationPageController::class)->index($response);
})->setName('authe.index');

$app->get('/autho', function(Request $request, Response $response) {
    return $this->get(AuthorizationPageController::class)->index($response);
})->setName('autho.index');

include ROOT_FOLDER . 'vendor/paneric/authentication/routes-common.php';
